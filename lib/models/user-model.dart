
class ClientData {
  List<User> users;
  String error;

  ClientData({this.users});

//  ClientData.fromJson(Map<String, dynamic> json) {
//    if (json['clients'] != null) {
//      clients = new List<User>();
//      json['clients'].forEach((v) {
//        clients.add(new User.fromJson(v));
//      });
//    }
//  }


  ClientData.fromJson(Map<String, dynamic> json)
      : users =
  (json['data']["clients"] as List).map((i) => new User.fromJson(i)).toList(),
        error = "";

  ClientData.withError(String errorValue)
      : users = List(),
        error = errorValue;


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['data']['clients'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class WorkerData {
  List<User> users;
  String error;

  WorkerData({this.users});

//  WorkerData.fromJson(Map<String, dynamic> json) {
//    if (json['clients'] != null) {
//      clients = new List<User>();
//      json['clients'].forEach((v) {
//        clients.add(new User.fromJson(v));
//      });
//    }
//  }


  WorkerData.fromJson(Map<String, dynamic> json)
      : users =
  (json['data']["workers"] as List).map((i) => new User.fromJson(i)).toList(),
        error = "";

  WorkerData.withError(String errorValue)
      : users = List(),
        error = errorValue;


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['data']['workers'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class User {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;
  int canConfirmPayments;
  int canCreateClients;
  int canCreateUsers;
  int createdById;
  String phone;
  String apiToken;
  String type;
  String fullName;

  User(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.canConfirmPayments,
        this.canCreateClients,
        this.canCreateUsers,
        this.createdById,
        this.phone,
        this.apiToken,
        this.type,
        this.fullName});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    canConfirmPayments = json['can_confirm_payments'];
    canCreateClients = json['can_create_clients'];
    canCreateUsers = json['can_create_users'];
    createdById = json['created_by_id'];
    phone = json['phone'];
    apiToken = json['access_token'];
    type = json['type'];
    fullName = json['full_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['can_confirm_payments'] = this.canConfirmPayments;
    data['can_create_clients'] = this.canCreateClients;
    data['can_create_users'] = this.canCreateUsers;
    data['created_by_id'] = this.createdById;
    data['phone'] = this.phone;
    data['access_token'] = this.apiToken;
    data['type'] = this.type;
    data['full_name'] = this.fullName;
    return data;
  }
}


class UserModel {
  int id;
  String name;
  String email;
  String paymentType;
  String accessToken;
  String tokenType;
  int expiresIn;

  UserModel(
      {
        this.id,
        this.name,
        this.email,
        this.paymentType,
        this.accessToken,
        this.tokenType,
        this.expiresIn
      });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    paymentType = json['payment_type'];
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['payment_type'] = this.paymentType;
    data['access_token'] = this.accessToken;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    return data;
  }
}



//        'user_id':'${userId.toString()}',
