
class IncomeOutcomeData {
  List<IncomeOutcome> income;
  List<IncomeOutcome> outcome;

  IncomeOutcomeData({this.income, this.outcome});

  IncomeOutcomeData.fromJson(Map<String, dynamic> json) {
    if (json['data']['income'] != null) {
      income = new List<IncomeOutcome>();
      json['data']['income'].forEach((v) {
        income.add(new IncomeOutcome.fromJson(v));
      });
    }
    if (json['data']['outcome'] != null) {
      outcome = new List<IncomeOutcome>();
      json['data']['outcome'].forEach((v) {
        outcome.add(new IncomeOutcome.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.income != null) {
      data['data']['income'] = this.income.map((v) => v.toJson()).toList();
    }
    if (this.outcome != null) {
      data['data']['outcome'] = this.outcome.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class IncomeOutcome {
  String name;
  int key;
  int total;

  IncomeOutcome({this.name, this.key, this.total});

  IncomeOutcome.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    key = json['key'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['key'] = this.key;
    data['total'] = this.total;
    return data;
  }
}
