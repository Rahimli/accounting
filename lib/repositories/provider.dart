import 'dart:async';
import 'dart:convert';
import 'dart:io';
//import 'package:http/http.dart' show Client;
import 'package:accounting/models/currency.dart';
import 'package:accounting/models/income-outcome.dart';
import 'package:accounting/models/payment.dart';
import 'package:accounting/models/user-model.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';


class DataProvider {

  Future<ClientData> fetchClientList() async {
    Dio dio = new Dio();
    Response response;
     print("-----------------------------------------");
    var form_data = {
      'user_type':'clients',
    };
    try {
      response = await dio.post("http://panel.vgashimov.webfactional.com/api/allusers",data: json.encode(form_data));
      if (response.statusCode == 200) {
        return ClientData.fromJson(response.data);
      } else {
        return ClientData.withError("error");
      }
    }on DioError catch(e, stacktrace) {
      return ClientData.withError("$e");
    }
  }

  Future<WorkerData> fetchWorkersList() async {
    Dio dio = new Dio();
    Response response;
     print("-----------------------------------------");
    var form_data = {
      'user_type':'workers',
    };
    try {
      response = await dio.post("http://panel.vgashimov.webfactional.com/api/allusers",data: json.encode(form_data));
      if (response.statusCode == 200) {
        return WorkerData.fromJson(response.data);
      } else {
        return WorkerData.withError("error");
      }
    }on DioError catch(e, stacktrace) {
      return WorkerData.withError("$e");
    }
  }

  Future<CurrencyData> fetchCurrenciesList() async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
     print("-----------------------------------------");
    var form_data = {
      "token":apiToken,
    };
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };
    try {
      dio.options.headers = headersMap;
      response = await dio.post("http://panel.vgashimov.webfactional.com/api/allcurrencies",data: json.encode(form_data));
      if (response.statusCode == 200) {
        return CurrencyData.fromJson(response.data);
      } else {
        return CurrencyData.withError("error");
      }
    }on DioError catch(e, stacktrace) {
      return CurrencyData.withError("$e");
    }
  }

  Future<PaymentData> fetchPaymentsList(DateTime from_date,DateTime to_date,int worker_id,int client_id) async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
     print("-----------------------------------------");
    var form_data = {
      "token":apiToken,
      "from_date":"${from_date.year}-${from_date.month.toString().length == 2?from_date.month:'0'+from_date.month.toString()}-${from_date.day.toString().length == 2?from_date.day:'0'+from_date.day.toString()}",
      "to_date":"${from_date.year}-${to_date.month.toString().length == 2?to_date.month:'0'+to_date.month.toString()}-${to_date.day.toString().length == 2?to_date.day:'0'+to_date.day.toString()}",
    };
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };

    try {
      dio.options.headers = headersMap;
      response = await dio.get("http://panel.vgashimov.webfactional.com/api/payments",queryParameters: form_data);
      print("--------------------------------------------------------");
      print(response.data);
      print("--------------------------------------------------------");
      if (response.statusCode == 200) {
        return PaymentData.fromJson(response.data);
      } else {
        return PaymentData.withError("error");
      }
    }on DioError catch(e, stacktrace) {
      return PaymentData.withError("$e");
    }
  }


  Future<PaymentData> fetchPaymentsDetailList(DateTime from_date,DateTime to_date,int worker_id,int client_id,String cw) async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
    print("-----------------------------------------");
    Map<String,dynamic> form_data = {
      "token":apiToken,
      "type":cw,
      "from_date":"${from_date.year}-${from_date.month.toString().length == 2?from_date.month:'0'+from_date.month.toString()}-${from_date.day.toString().length == 2?from_date.day:'0'+from_date.day.toString()}",
      "to_date":"${from_date.year}-${to_date.month.toString().length == 2?to_date.month:'0'+to_date.month.toString()}-${to_date.day.toString().length == 2?to_date.day:'0'+to_date.day.toString()}",
    };
    if(cw == 'client'){
      form_data['client_id'] = client_id;
    }else{
      form_data['worker_id'] = worker_id;
    }
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };

    try {
      dio.options.headers = headersMap;
      response = await dio.get("http://panel.vgashimov.webfactional.com/api/payments",queryParameters: form_data);
      print("------------------------------------------------------12--");
      print(response.data);
      print("------------------------------------------------------12--");
      if (response.statusCode == 200) {
        return PaymentData.fromJson(response.data);
      } else {
        return PaymentData.withError("error");
      }
    }on DioError catch(e, stacktrace) {
      return PaymentData.withError("$e");
    }
  }



  Future<IncomeOutcomeData> fetchDashboardData(DateTime from_date,DateTime to_date, int client_id) async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
     print("-----------------------------------------");
    var form_data = {
      "token":apiToken,
      "client_id":client_id,
      "from_date":"${from_date.year}-${from_date.month.toString().length == 2?from_date.month:'0'+from_date.month.toString()}-${from_date.day.toString().length == 2?from_date.day:'0'+from_date.day.toString()}",
      "to_date":"${from_date.year}-${to_date.month.toString().length == 2?to_date.month:'0'+to_date.month.toString()}-${to_date.day.toString().length == 2?to_date.day:'0'+to_date.day.toString()}",
    };
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };

    try {
      dio.options.headers = headersMap;
      response = await dio.get("http://panel.vgashimov.webfactional.com/api/dashboardapi",queryParameters: form_data);
      print("--------------------------------------------------------");
      print(response.data);
      print("--------------------------------------------------------");
      if (response.statusCode == 200) {
        return IncomeOutcomeData.fromJson(response.data);
      } else {
        return null;
      }
    }on DioError catch(e, stacktrace) {
      throw e;
    }
  }

  Future<bool> addPayment(int client_id,List<Map> amounts,bool received,User user) async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
     print("-----------------------------------------");
    var form_data = {
      "token":apiToken,
      "client_id":client_id,
      "received":received,
      "amounts":amounts,
      "confirmed":"confirmed",
      "worker_id":user.id,
      "currency_id":1
    };
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };
    try {
      dio.options.headers = headersMap;
      response = await dio.post("http://panel.vgashimov.webfactional.com/api/payments",data: json.encode(form_data));
      if (response.statusCode == 200) {
        print(response.data);
        return true;
      } else {
        return false;
      }
    }on DioError catch(e, stacktrace) {
      return false;
    }
  }
  Future<bool> confirmPayment(int index, int id,int client_id,String type) async {
    Dio dio = new Dio();
    Response response;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('apiToken').toString();
     print("-----------------------------------------");
    var form_data = {
      "token":apiToken,
      "id":id,
      "client_id":client_id,
      "confirmed":type,
    };
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + apiToken,
    };
    try {
      dio.options.headers = headersMap;
      response = await dio.post("http://panel.vgashimov.webfactional.com/api/payment-update",data: json.encode(form_data));
      if (response.statusCode == 200) {
        print(response.data);
        return true;
      } else {
        return false;
      }
    }on DioError catch(e, stacktrace) {
      print("e.messagee.messagee.messagee.messagee.messagee.message");
      print(e.message);
      return false;
    }
  }

}


