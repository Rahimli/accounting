import 'dart:async';

import 'package:accounting/models/currency.dart';
import 'package:accounting/models/payment.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/repositories/provider.dart';

class Repository {
  final dataProvider = DataProvider();

  Future<ClientData> fetchAllClients() => dataProvider.fetchClientList();
  Future<WorkerData> fetchAllWorkers() => dataProvider.fetchWorkersList();
  Future<WorkerData> fetchMyData() => dataProvider.fetchWorkersList();
  Future<CurrencyData> fetchCurrencies() => dataProvider.fetchCurrenciesList();
  Future<PaymentData> fetchPayments(DateTime from_date,DateTime to_date,int worker_id,int client_id) => dataProvider.fetchPaymentsList(from_date, to_date, worker_id, client_id);
  Future<PaymentData> fetchPaymentsDetailList(DateTime from_date,DateTime to_date,int worker_id,int client_id,String cw) => dataProvider.fetchPaymentsDetailList(from_date, to_date, worker_id, client_id,cw);
  Future fetchDashboardData(DateTime from_date,DateTime to_date,int client_id) => dataProvider.fetchDashboardData(from_date, to_date, client_id);
  Future<bool> addPayment(int client_id,List<Map> amounts,bool received,User user) => dataProvider.addPayment(
      client_id, amounts, received, user
  );
//  Future<CategoryData> fetchAllCategories() => dataProvider.fetchCategoryList();
//  Future<AppTariffType> fetchAppTariffType() => dataProvider.fetchAppTariffType();
//
//  Future<QuestionData> fetchQuestions(List<int> categoryIds) => dataProvider.fetchQuestion(categoryIds);
//  Future<ResultQuestionData> fetchResultQuestions(String sessionId) => dataProvider.fetchResultQuestion(sessionId);
//  Future<bool> fetchExamFinish(String sessionId,List<int> question_list,Map<String,int> my_answer_map) => dataProvider.fetchExamFinish(sessionId,question_list,my_answer_map);
}
