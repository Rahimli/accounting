import 'package:flutter/material.dart';

import 'loading.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Center(
    child: LoadingPage(),
  );
}

