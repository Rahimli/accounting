import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/customers/bloc_provider.dart';
import 'package:accounting/pages/home/customers/dashboard/dashboard.dart';
import 'package:accounting/pages/home/customers/forcustomer/list/bloc_provider.dart';
import 'package:accounting/pages/home/customers/forcustomer/list/index.dart';
import 'package:accounting/pages/home/customers/screen.dart';
import 'package:accounting/pages/home/dashboard/bloc_provider.dart';
import 'package:accounting/pages/home/dashboard/dashboard.dart';
import 'package:accounting/pages/home/employees/bloc_provider.dart';
import 'package:accounting/pages/home/employees/screen.dart';
import 'package:accounting/pages/home/payments/bloc_provider.dart';
import 'package:accounting/pages/home/payments/screen.dart';
import 'package:accounting/pages/home/settings/list/bloc_provider.dart';
import 'package:accounting/pages/home/settings/list/screen.dart';
import 'package:accounting/pages/login/login-page.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:accounting/repositories/user-repostiory.dart';
import 'package:accounting/pages/authentication/authentication.dart';
import 'package:accounting/pages/home/widgets/credit_card_widget.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentTabIndex = 0;

  User currentUser ;
  bool user_loading = true;
  UserRepository userRepository = UserRepository();
  _getUser() async {

    UserRepository repository = UserRepository();
    User client_result = await repository.getUser();
    if(client_result.id != '' && client_result.id != null){
      setState(() {
        currentUser = client_result;
        user_loading = false;
      });
    }else{

    }

  }
  @override
  void initState() {
    super.initState();

    _getUser();

  }

  static dashboardPage() {
    final page = DashboardPageBlocProvider(
      child: DashboardPage(
      ),
    );
    return page;
  }
  static customersPage() {
    final page = CustomersPageBlocProvider(
      child: CustomersPage(
      ),
    );
    return page;
  }
  static employeesPage() {
    final page = EmployeesPageBlocProvider(
      child: EmployeesPage(
      ),
    );
    return page;
  }
  static paymentsPage() {
    final page = PaymentsPageBlocProvider(
      child: PaymentsPage(
      ),
    );
    return page;
  }
  static userPaymentsPage(String type) {
    final page = PaymentForUserPageBlocProvider(
      child: PaymentForUserPage(
        type: type,
      ),
    );
    return page;
  }
  static settingsPage() {
    final page = SettingsPageBlocProvider(
      child: SettingsPage(
      ),
    );
    return page;
  }
  List<Widget> tabs = [
//    dashboardPage(),
    DashboardPage(),
    customersPage(),
    paymentsPage(),
    employeesPage(),
    settingsPage(),
  ];
  List<Widget> customerTabs = [
    CustomerDashboardPage(),
    userPaymentsPage('client'),
    settingsPage(),
  ];
  List<Widget> workerTabs = [
//    dashboardPage(),
    DashboardPage(),
    paymentsPage(),
    settingsPage(),
  ];

  var adminNavbar = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text("İzahat"),
    ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people),
          title: Text("Müştərilər"),
        ),
    BottomNavigationBarItem(
      icon: Icon(Icons.insert_chart),
      title: Text("Ödənişlər"),
    ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          title: Text("İşçilər"),
        ),
    BottomNavigationBarItem(
      icon: Icon(Icons.settings),
      title: Text("Ayarlar"),
    ),
  ];
  var workerNavbar = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text("İzahat"),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.insert_chart),
      title: Text("Ödənişlər"),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.settings),
      title: Text("Ayarlar"),
    ),
  ];
  var customerNavbar = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text("İzahat"),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.insert_chart),
      title: Text("Ödənişlər"),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.settings),
      title: Text("Ayarlar"),
    ),
  ];

  onTapped(int index) {
    setState(() {
      currentTabIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {

    final AuthenticationBloc authenticationBloc =
    BlocProvider.of<AuthenticationBloc>(context);

    return
      user_loading ? LoadingPage():
      Scaffold(
        backgroundColor: Color(0xfffafafa),
        bottomNavigationBar:
        BottomNavigationBar(
          fixedColor: Colors.cyan,
          type: BottomNavigationBarType.fixed,
          onTap: onTapped,
          currentIndex: currentTabIndex,
          items: currentUser.type == 'admin'?adminNavbar:currentUser.type == 'worker'?workerNavbar:customerNavbar,
        ),
        body: currentUser.type == 'admin'?tabs[currentTabIndex]:currentUser.type == 'worker'?workerTabs[currentTabIndex]:customerTabs[currentTabIndex],
      );


  }
}
