import 'package:accounting/pages/common/alert.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:flutter/services.dart';



class EmployeeAddPage extends StatefulWidget {
  @override
  _EmployeeAddPageState createState() => _EmployeeAddPageState();
}

class _EmployeeAddPageState extends State<EmployeeAddPage> {
  bool _loading = false;
  String _errorMessage = '';

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 18.0,color: Colors.black87);
  final _formKey = GlobalKey<FormState>();
  final FocusNode _emailFocus = FocusNode();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _numberController = TextEditingController();


  createUser() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _loading = true;
      });
      List<dynamic> result = [false,''];
      UserRepository userRepository = UserRepository();
      try {
        result = await userRepository.signUp(
          email: _emailController.text,
          name: _nameController.text,
          number: _numberController.text,
//          number: _p.text,
          type: 'worker',
        );
//
//        print("%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%");
//        print(result);
//        print("%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%%^&***^*&^%^&%");
        if(result[0]){
          Navigator.pop(context);
        }else{
          GeneralAlerts.showErrorMessage(context,"Bu email artıq istifadə olunub");
        }
        setState(() {
          _errorMessage = '';
          _loading = false;
        });

      } on PlatformException catch (e) {
        setState(() {
          _errorMessage = e.message;
          _loading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Yeni işçi'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: _errorMessage.isNotEmpty
                  ? Text(
                _errorMessage,
                style: TextStyle(color: Colors.red),
                textAlign: TextAlign.center,
              )
                  : Container(),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'İşçi adını daxil etmədiniz';
                  }
                },
                controller: _nameController,
                textInputAction: TextInputAction.next,
                textCapitalization: TextCapitalization.words,
                onFieldSubmitted: (name) {
                  FocusScope.of(context).requestFocus(_emailFocus);
                },
                style: style,
                decoration: InputDecoration(
                  hintText: "İşçi adı",
                  filled: true,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Emaili daxil etmədiniz';
                  }

                  if (!isEmail(value)) {
                    return 'Email düzgün formatda deyil';
                  }
                },
                controller: _emailController,
                textInputAction: TextInputAction.next,
                focusNode: _emailFocus,
                keyboardType: TextInputType.emailAddress,
                onFieldSubmitted: (email) => createUser(),
                style: style,
                decoration: InputDecoration(
                  hintText: "Email",
                  filled: true,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Nömrə daxil etmədiniz';
                  }

                  if (value.length<7) {
                    return 'Nömrə düzgün formatda deyil';
                  }
                },
                controller: _numberController,
                textInputAction: TextInputAction.next,
//                focusNode: _emailFocus,
                keyboardType: TextInputType.number,
                onFieldSubmitted: (email) => createUser(),
                style: style,
                decoration: InputDecoration(
                  hintText: "Nömrə",
                  filled: true,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),
                ),
              ),
            ),
            Material(
              elevation: 5.0,
//              borderRadius: BorderRadius.circular(30.0),
              color: Colors.cyan,
              child: MaterialButton(
                key: Key('signIn'),
                onPressed: _loading ? null : createUser,
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                child: _loading ? CircularProgressIndicator() : Text( 'Əlavə et',
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 15.0)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
