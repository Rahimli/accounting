
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/employees/bloc.dart';
import 'package:accounting/pages/home/employees/bloc_provider.dart';
import 'package:accounting/pages/home/employees/detail/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'add/screen.dart';
import 'detail/detail.dart';

class EmployeesPage extends StatefulWidget {
  @override
  _EmployeesPageState createState() => _EmployeesPageState();
}

class _EmployeesPageState extends State<EmployeesPage> {
  EmployeesPageBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = EmployeesPageBlocProvider.of(context);
    bloc.fetchAllEmployees();
  }

  static paymentDetailPage(int employee_id,String employee_name,BuildContext context) {
    final page = PaymentDetailPageBlocProvider(
      child: PaymentDetailPage(
        employee_id: employee_id,
        employee_name: employee_name,
      ),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
//        iconTheme: IconThemeData(color: Colors.black),
//        brightness: Brightness.light,
//        backgroundColor: Colors.white,
//        elevation: 0.3,
        title: Text("İşçilər",style: TextStyle(inherit: true,color: Colors.black),),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.cyan,
        onPressed: (){

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EmployeeAddPage()),
          );
        },
      ),
      body: StreamBuilder<WorkerData>(
        stream: bloc.subject.stream,
        builder: (context, AsyncSnapshot<WorkerData> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0){
              return _buildErrorWidget(snapshot.data.error);
            }
            return _buildMainDataWidget(snapshot.data);

          } else if (snapshot.hasError) {
            return _buildErrorWidget(snapshot.error);
          } else {
            return LoadingPage();
          }
        },
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Text("Xəta baş verdi, təkrar cəhd edin"),
    );
  }




  Widget _buildMainDataWidget(WorkerData data) {
    return data.users.length == 0 ? Center(child: Text("İşçi yoxdur"),):ListView.builder(
      itemCount: data.users.length,
      itemBuilder: (BuildContext context,int index){
        User user_item = data.users[index];
        return Card(
          child: ListTile(
            title: Text("${user_item.fullName}",),
            subtitle: Text('${user_item.email}'),
            trailing: Icon(Icons.keyboard_arrow_right),
            leading: Icon(Icons.person),
            onTap: (){
              paymentDetailPage(user_item.id,user_item.fullName,context);
            },
          ),
        );
      },
    );
  }
}

