import 'package:accounting/pages/home/employees/bloc.dart';
import 'package:flutter/material.dart';



class EmployeesPageBlocProvider extends InheritedWidget {
  final EmployeesPageBloc bloc;

  EmployeesPageBlocProvider({Key key, Widget child})
      : bloc = EmployeesPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static EmployeesPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(EmployeesPageBlocProvider)
    as EmployeesPageBlocProvider)
        .bloc;
  }
}
