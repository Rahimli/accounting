import 'package:accounting/models/income-outcome.dart';
import 'package:flutter/material.dart';


class CreditCardWidget extends StatelessWidget{
  final Color themeColor;
  final String title;
  final List<IncomeOutcome> value;
  final String value_azn;
  final String value_usd;
  const CreditCardWidget({Key key, this.themeColor=Colors.white, this.title, this.value_azn,this.value_usd, this.value, }) : super(key: key);
  @override
  Widget build(BuildContext context) {
     return _buildExpanded(context);
  }
  Widget _buildExpanded(BuildContext context)
  {
    double rowWidth=MediaQuery.of(context).size.width*2/3;
    Color color=themeColor==Colors.white?Colors.black:Colors.white;
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 28.0,horizontal: 24.0),
      decoration: BoxDecoration(
        color: themeColor,
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: [BoxShadow(color: Colors.pink.shade100,blurRadius: 200.0,spreadRadius: 0.1,offset: Offset(0.0, 10.0)),BoxShadow(color: Colors.white,blurRadius: 300.0,spreadRadius: 10.5)],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("$title",style: Theme.of(context).textTheme.title.copyWith(fontSize: 16.0,color: color),),
//              Image.network(card.logo,height:18.0,fit: BoxFit.fitHeight,color:themeColor==Colors.white?null:Colors.white,),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: value.map((item) =>
              Text("${item.total.toString()} ${item.name.toUpperCase()}", style: Theme
                  .of(context)
                  .textTheme
                  .headline
                  .copyWith(color: color,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0),)).toList()
            ),
          ),
        ],
      ),
    );
  }
}