import 'package:accounting/pages/common/alert.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/common/validator.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';



class ProfileChangePage extends StatefulWidget {
  final String username;

  const ProfileChangePage({Key key, this.username}) : super(key: key);
//  const ProfileChangePage();

  @override
  State<StatefulWidget> createState() => _ProfileChangePageState();
}

enum FormType {
  login,
}
LoginRegisterValidator loginRegisterValidator = LoginRegisterValidator();
class _ProfileChangePageState extends State<ProfileChangePage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController _passwordController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 18.0,color: Colors.black87);

  @override
  void initState() {
    super.initState();
  }
  String _name;
  String _email;
  String _password;
  String _retypePassword;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }



  void _showFormError(BuildContext context,String message) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Error"),
          content: Text("${message}"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        )
    );
  }

  Future<void> validateAndSubmit() async {
    // print("--------------&&*&*&*&&----------------------");
    // print(validateAndSave());
    // print("--------------&&*&*&*&&----------------------");
    if (validateAndSave()) {
      LoadingDialog.showFormLoadingDialog(context, "Please wait");
      try {
//        var dio = new Dio();
//        await dio.get("https://wendux.github.io/xsddddd");

        final UserRepository userRepository = UserRepository();
        if (_formType == FormType.login) {
          if((_password.trim().length == 0 && _retypePassword.trim().length == 0 ) || (_password.trim().length > 0 && _retypePassword.trim().length > 6)){
            final String response = await userRepository.changeProfile(_name, _password, _retypePassword);
             print('Signed in: $response');
             if(response == 'token') {
                Navigator.pop(context);
                Navigator.pop(context);
             }else{
               Navigator.pop(context);
               GeneralAlerts.showErrorMessage(context,"Düzgün şifrə daxil edin");
             }
           }else{
            Navigator.pop(context);
            if(_password.trim().length == 0)
            GeneralAlerts.showErrorMessage(context,"Şifrə daxil etmədiniz");
            else if( _retypePassword.trim().length < 7)
              GeneralAlerts.showErrorMessage(context,"Yeni şifrə 6 simvoldan çox olmalıdır");
          }
        }
//        widget.onSignedIn();
      } on DioError catch(e) {
        if(e.response.statusCode == 302){
          // print("Error ${e.response.statusCode}");
          Navigator.pop(context);
          GeneralAlerts.showErrorMessage(context,"Düzgün şifrə daxil edin");
        }
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      AppBar(
        title: Text("Profil tənzimi"),
        centerTitle: true,
//                            shape: BeveledRectangleBorder(side: ),
      ),
      body:
      Form(
        key: formKey,
        child: ListView(
//          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 15.0),
            Padding(
              padding: const EdgeInsets.only(
                  top: 5.0, bottom: 5.0, left: 20.0, right: 20.0),
              child: TextFormField(
                key: Key('name'),
                validator: loginRegisterValidator.validateName,
                initialValue: widget.username,
                onSaved: (String value) => _name = value,
                // validator: validator.validateEmail,

                obscureText: false,
//                controller: _nameController,


                style: style,
                decoration: InputDecoration(

//                  fillColor: Colors.black87,
                  hintText: "Ad",
                  filled: true,
//                  fillColor: Color(0xff3F458F),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(15.0)
//                      ),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),

                ),
              ),
            ),
            SizedBox(height: 3.0),
            Padding(
              padding: const EdgeInsets.only(
                  top: 5.0,
                  bottom: 10.0,
                  left: 20.0,
                  right: 20.0),
              child: TextFormField(
                key: Key('password'),
                controller: _passwordController,
//                validator: loginRegisterValidator.validatePassword,
                onSaved: (String value) => _password = value,
                obscureText: true,
                style: style,
                decoration: InputDecoration(

//                  fillColor: Colors.black87,
                  hintText: "Şifrə",
                  filled: true,
//                  fillColor: Color(0xff3F458F),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(15.0)
//                      ),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),

                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: const EdgeInsets.only(
                  top: 5.0,
                  bottom: 10.0,
                  left: 20.0,
                  right: 20.0),
              child: TextFormField(
                key: Key('confirm_password'),
//                validator: (value) => loginRegisterValidator.validateRetypePassword(value, _passwordController.text),
                onSaved: (String value) => _retypePassword = value,
                obscureText: true,

                style: style,
                decoration: InputDecoration(

//                  fillColor: Colors.black87,
                hintText: "Yeni şifrə",
                  filled: true,
//                  fillColor: Color(0xff3F458F),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                  focusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(15.0)
//                      ),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))
                  ),
                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide(width: 1,color: Color(0xff3F458F))),

                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0,
                  bottom: 10.0,
                  left: 20.0,
                  right: 20.0),
              child: Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: Colors.cyan,
                child: MaterialButton(
                  key: Key('change'),
                  onPressed: validateAndSubmit,
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  child: Text( 'Dəyiş',
                      textAlign: TextAlign.center,
                      style: style.copyWith(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 15.0)
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}