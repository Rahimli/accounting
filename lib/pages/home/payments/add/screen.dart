import 'package:accounting/components/datetime_picker.dart';
import 'package:accounting/models/currency.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/alert.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:validators/validators.dart';

class PaymentCreatePage extends StatefulWidget {
  @override
  _PaymentCreatePageState createState() => _PaymentCreatePageState();
}

class _PaymentCreatePageState extends State<PaymentCreatePage> {
  bool _loading = false;
  String _errorMessage = '';
  bool _received = true;


  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 18.0,color: Colors.black87);
  User _selectedClient;
  List<User> _clients = [];
  List<CurrencyModel> _currencies = [];

  final _formKey = GlobalKey<FormState>();
  List<TextEditingController> _amountControllers = [];
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  DateTime _date = DateTime.now();

  @override
  void initState() {
    super.initState();

    _getCurrencies();
    _getClients();

  }

  _getClients() async {

    Repository repository = Repository();
    ClientData client_result = await repository.fetchAllClients();
//    List<UserModel> clients = [];

    setState(() {
      _clients = client_result.users;
    });
  }

  _getCurrencies() async {
    Repository repository = Repository();
    CurrencyData client_result = await repository.fetchCurrencies();
    for(int i=0; i<client_result.currencies.length; i++){
      _amountControllers.add(TextEditingController(text: '0'));
    };
    setState(() {
      _currencies = client_result.currencies;
    });

  }

  _createPayment() async {
    if (_selectedClient == null) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text("Müştəri seçin"),
        ),
      );

      return null;
    }

    if (_formKey.currentState.validate()) {
      setState(() {
        _loading = true;
      });

      try {
        List<Map> amounts = [];

        for (var i = 0; i < _currencies.length; i++) {
          print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
          print(_amountControllers[i].text);
          print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
          int amount = int.parse(_amountControllers[i].text);

          if (amount > 0) {
            amounts.add({
              'amount': int.parse(_amountControllers[i].text),
              'currency_id': _currencies[i].id,
            });
          }
        }
        Repository repository = Repository();
        UserRepository userRepository = UserRepository();
        if(amounts.length>0) {
          print("%^%^%^%^%^%^%^%^%^%^%^%^%%^%^%^%^%^%^%^%^%^%^%^%^%%^%^%^%^%^%^%^%^%^%^%^%^%");
          print(amounts);
          print("%^%^%^%^%^%^%^%^%^%^%^%^%%^%^%^%^%^%^%^%^%^%^%^%^%%^%^%^%^%^%^%^%^%^%^%^%^%");
          User user = await userRepository.getUser();
          bool add_payment = await repository.addPayment(
              _selectedClient.id, amounts, _received, user);
          if (add_payment) {
            setState(() {
              _errorMessage = '';
              _loading = false;
            });
            Navigator.pop(context);
          } else {
            setState(() {
              _errorMessage = 'Düzgün məlumat daxil edin';
              _loading = false;
            });
            Navigator.pop(context);
          }
        }else{
          _errorMessage = '';
          _loading = false;
          GeneralAlerts.showErrorMessage(context,"Məbləğlərdən heç olmasa biri daxil edilməidir");
        }
      } on PlatformException catch (e) {
        setState(() {
          _errorMessage = e.message;
          _loading = false;
        });
      }
    }
  }

  _onReceivedChanged(bool val) {
    setState(() {
      _received = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Yeni Ödəniş'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: _errorMessage.isNotEmpty
                  ? Text(
                _errorMessage,
                style: TextStyle(color: Colors.red),
                textAlign: TextAlign.center,
              )
                  : Container(),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                height: 60,
                child: DropdownButton<User>(
                  hint: Text("Müştəri seçin"),
                  isExpanded: true,
                  value: _selectedClient,
                  onChanged: (newClient) {
                    setState(() {
                      _selectedClient = newClient;
                    });
                  },
                  items: _clients.map((client) {
                    return DropdownMenuItem(
                      value: client,
                      child: Text(client.fullName),
                    );
                  }).toList(),
                ),
              ),
            ),
            Column(
              children: <Widget>[
                for (var i = 0; i < _currencies.length; i++)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Məbləğ',
                        suffixIcon: Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: Text(_currencies[i].title),
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      controller: _amountControllers[i],
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Məbləğ yazın";
                        }

                        if (!isInt(val)) {
                          return "Məbləğ düzgün deyil.";
                        }
                      },
                    ),
                  ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("Ödəniş alınır"),
                      Radio(
                        value: true,
                        groupValue: _received,
                        onChanged: _onReceivedChanged,
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text("Ödəniş edilir"),
                      Radio(
                        value: false,
                        groupValue: _received,
                        onChanged: _onReceivedChanged,
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: DateTimePicker(
                labelText: 'Başlanğıc tarix',
                selectedDate: _date,
                selectDate: (DateTime date) {
                  setState(() {
                    _date = date;
                  });
                },
              ),
            ),
            Material(
              elevation: 5.0,
//              borderRadius: BorderRadius.circular(30.0),
              color: Colors.cyan,
              child: MaterialButton(
                key: Key('signIn'),
                onPressed: _loading ? null : _createPayment,
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                child: _loading ? CircularProgressIndicator() : Text( 'Əlavə et',
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 15.0)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
