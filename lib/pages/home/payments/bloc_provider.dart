import 'package:accounting/pages/home/employees/bloc.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';



class PaymentsPageBlocProvider extends InheritedWidget {
  final PaymentsPageBloc bloc;

  PaymentsPageBlocProvider({Key key, Widget child})
      : bloc = PaymentsPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static PaymentsPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(PaymentsPageBlocProvider)
    as PaymentsPageBlocProvider)
        .bloc;
  }
}
