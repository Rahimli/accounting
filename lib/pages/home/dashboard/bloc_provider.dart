import 'package:accounting/pages/home/customers/bloc.dart';
import 'package:accounting/pages/home/dashboard/bloc.dart';
import 'package:flutter/material.dart';



class DashboardPageBlocProvider extends InheritedWidget {
  final DashboardPageBloc bloc;

  DashboardPageBlocProvider({Key key, Widget child})
      : bloc = DashboardPageBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static DashboardPageBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(DashboardPageBlocProvider)
    as DashboardPageBlocProvider)
        .bloc;
  }
}
