import 'package:accounting/models/user-model.dart';
import 'package:accounting/repository/user-repository.dart';
import 'package:rxdart/rxdart.dart';

class DashboardPageBloc {

  final Repository _repository = Repository();
  final BehaviorSubject<ClientData> _subject =
  BehaviorSubject<ClientData>();

//  Observable<ClientData> get subject => _subject.stream;

  fetchAllCustomers() async {
    ClientData customerData = await _repository.fetchAllClients();
    print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    print(customerData.toString());
    _subject.sink.add(customerData);
  }



  BehaviorSubject<ClientData> get subject => _subject;

  dispose() {
    _subject.close();
  }
}

//final categories_bloc = DashboardPageBloc();
