import 'package:accounting/components/datetime_picker.dart';
import 'package:accounting/models/payment.dart';
import 'package:accounting/models/user-model.dart';
import 'package:accounting/pages/common/loading.dart';
import 'package:accounting/pages/home/payments/add/screen.dart';
import 'package:accounting/repositories/provider.dart';
import 'package:accounting/repositories/user-repostiory.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';
import 'bloc_provider.dart';


class PaymentForUserPage extends StatefulWidget {
  final String type;

  const PaymentForUserPage({Key key, this.type}) : super(key: key);
  @override
  _PaymentForUserPageState createState() => _PaymentForUserPageState();
}

class _PaymentForUserPageState extends State<PaymentForUserPage> {
  int user_id = 0;

  List<Widget> list_payments = new List<Widget>();


  User currentUser ;
  bool user_loading = true;
  UserRepository userRepository = UserRepository();
  DataProvider dataProvider = DataProvider();
  _getUser() async {

    UserRepository repository = UserRepository();
    User client_result = await repository.getUser();
    if(client_result.id != '' && client_result.id != null){
      setState(() {
        currentUser = client_result;
        user_id = client_result.id;
        user_loading = false;
        print("falsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalsefalse");
      });
    }else{

    }

  }
  
  
  DateTime _fromDate = DateTime(

    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day - 10,
    0,
    0,
    0,
  );
  DateTime _toDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
    23,
    59,
    59,
  );
  bool showFilter = false;


  PaymentForUserPageBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = PaymentForUserPageBlocProvider.of(context);
    _getUser();
    if(!user_loading){
    if(widget.type == 'worker'){
      bloc.fetchAllPayments(_fromDate,_toDate,user_id,0,"worker");
    }else {
      bloc.fetchAllPayments(_fromDate, _toDate, 0, user_id, "client");
      print("bloc.fetchAllPayments(_fromDate, _toDate, 0, user_id,client)");
    }
  }
  }

  @override
  Widget build(BuildContext context) {
    if(!user_loading)
    widget.type == 'worker'?bloc.fetchAllPayments(_fromDate,_toDate,user_id,0,"worker"):bloc.fetchAllPayments(_fromDate, _toDate, 0, user_id, "client");

    print("bloc.fetchAllPayments(_fromDate, _toDate, 0, user_id,client)");
    return user_loading?LoadingPage():Scaffold(
      appBar: AppBar(
        title: Text("Ödənişlər"),
        centerTitle: true,
      ),
      body:
      Container(
        child:
        Padding(
//              padding: const EdgeInsets.only(bottom: 20,left: 20,right: 20),
          padding: const EdgeInsets.all(0),
          child: Column(
            children: <Widget>[
              Card(
                child: ListTile(
                  onTap: (){
                    setState(() {
                      showFilter = !showFilter;
                    });},
                  title: Text("Filtr"),
                  leading: Icon(Icons.filter_list),
                  trailing: showFilter ? Icon(Icons.keyboard_arrow_up):Icon(Icons.keyboard_arrow_down),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 20,left: 20,right: 20),
                child:
                showFilter ?
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: DateTimePicker(
                        labelText: 'Başlanğıc tarix',
                        selectedDate: _fromDate,
                        selectDate: (DateTime date) {
                          print(date);
                          setState(() {
                            _fromDate = date;
                          });
                        },
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: DateTimePicker(
                        labelText: 'Son tarix',
                        selectedDate: _toDate,
                        selectDate: (DateTime date) {
                          setState(() {
                            _toDate = DateTime(
                              date.year,
                              date.month,
                              date.day,
                              23,
                              59,
                              59,
                            );
                          });
                        },
                      ),
                    ),
                  ],
                )
                    :SizedBox(),
              ),
              Expanded(
                child:StreamBuilder<PaymentData>(
                  stream: bloc.subject.stream,
                  builder: (context, AsyncSnapshot<PaymentData> snapshot) {
                    print("StreamBuilderStreamBuilderStreamBuilderStreamBuilderStreamBuilderStreamBuilder");
                    if (snapshot.hasData) {
                      if (snapshot.data.error != null && snapshot.data.error.length > 0){
                        return _buildErrorWidget(snapshot.data.error);
                      }
                      return _buildMainDataWidget(snapshot.data);

                    } else if (snapshot.hasError) {
                      return _buildErrorWidget(snapshot.error);
                    } else {
                      return LoadingPage();
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildErrorWidget(Object error) {
    return Center(
      child: Text("Xəta baş verdi, təkrar cəhd edin"),
    );
  }

  Widget _buildMainDataWidget(PaymentData data) {

    return data.payments.length == 0 ? Center(child: Text("Ödəniş yoxdur"),):
    ListView.builder(
      itemCount: data.payments.length,
      itemBuilder: (BuildContext context ,int index){
        Payments payment_item = data.payments[index];
        return Card(
          child: Column(
            children: <Widget>[
              ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: payment_item.amounts.map((item) => Text("${item.amount} ${item.currencyTitle}")).toList(),
                ),
                leading: Icon(payment_item.received == 1 ?Icons.arrow_downward:Icons.arrow_upward,color: payment_item.received == 1 ? Colors.green : Colors.red,),
                trailing: payment_item.confirmed == 'confirmed'?IconButton(icon: Icon(Icons.check_circle_outline,color: Colors.green),) : payment_item.confirmed == 'rejected'?IconButton(icon: Icon(Icons.remove_circle_outline,color: Colors.red,),):IconButton(icon: Icon(Icons.access_time,color: Colors.amber,),),
            subtitle: Text('${payment_item.date.toString()}'),
              ),
              widget.type == 'client'?
              payment_item.confirmed == 'confirmed' || payment_item.confirmed == 'rejected' ?
              SizedBox(height: 0,):
              Row(
                children: <Widget>[
                  Expanded(
                    child:
                    GestureDetector(
                      onTap: (){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title:  Center(child: Text("Əminisiniz")),
                              content: new Text("Təsdiq etmək istədiyinizdən əminsiniz?"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Bağla",style: TextStyle(color: Colors.amber),),
                                  onPressed: () {

                                    Navigator.pop(context);
                                  },
                                ),
                                new FlatButton(
                                  child: Text("Təsdiq et",style: TextStyle(color: Colors.green),),
                                  onPressed: () {
                                    paymentConfirmation(index,payment_item.id,currentUser.id,'confirmed',context);

                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: Container(
                        color: Colors.green,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Icon(Icons.check_circle,color: Colors.white,size: 28,),
                          )),
                    ),
                  ),
                  Expanded(
                    child:
                    GestureDetector(
                      onTap: (){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title:  Center(child: Text("Əminisiniz")),
                              content: new Text("İmtina etmək istədiyinizdən əminsiniz?"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Bağla",style: TextStyle(color: Colors.amber),),
                                  onPressed: () {

                                    Navigator.pop(context);
                                  },
                                ),
                                new FlatButton(
                                  child: Text("İmtina et",style: TextStyle(color: Colors.red),),
                                  onPressed: () {
                                    paymentConfirmation(index,payment_item.id,currentUser.id,'rejected',context);

                                  },
                                ),
                              ],
                            );
                          },
                        );
                        },
                      child: Container(
                        color: Colors.red,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Icon(Icons.cancel,color: Colors.white,size: 28,),
                          )),
                    ),
                  ),
                ],
              )
              :SizedBox(height: 0,)
            ],
          ),
        );
      },
    );
  }

  void paymentConfirmation(int index, int id,int client_id,String type,BuildContext context) async{
//    Navigator.pop(context);
    LoadingDialog.showFormLoadingDialog(context, "Please wait");
    bool result = await dataProvider.confirmPayment(index, id, client_id, type);
    Navigator.of(context).pop();
    Navigator.of(context).pop();
//    Navigator.pop(context);
    print("resultresultultresultresultresultresultresultresult");
    print(result);
    print("resultresultultresultresultresultresultresultresult");
    setState(() {
      list_payments = [];
    });
  }
}
