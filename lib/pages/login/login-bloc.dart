import 'dart:async';

import 'package:accounting/models/user-model.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:accounting/repositories/user-repostiory.dart';
import 'package:accounting/pages/login/login.dart';
import 'package:accounting/pages/authentication/authentication.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final UserModel userModel = await userRepository.authenticate(
          email: event.username,
          password: event.password,
        );

        authenticationBloc.dispatch(LoggedIn(token: userModel.accessToken));
        yield LoginInitial();
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}