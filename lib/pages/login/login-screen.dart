import 'package:accounting/pages/payments/index.dart';
import 'package:flutter/material.dart';

class LoginScreenPage extends StatefulWidget {
  @override
  _LoginScreenPageState createState() => new _LoginScreenPageState();
}

class _LoginScreenPageState extends State<LoginScreenPage> {



  @override
  void initState() {
    super.initState();


  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:

      Container(
        decoration: BoxDecoration(
          color: Colors.cyan,
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.1), BlendMode.dstATop),
            image: AssetImage('assets/images/mountains.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          children: <Widget>[

            Container(
              child: new Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 110.0),
                    child: Center(
                      child: Icon(
                        Icons.show_chart,
                        color: Colors.white,
                        size: 80.0,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Hesabat ",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          "App",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 80.0),
                    child:
                    BeautyTextfield(
                      width: double.maxFinite,
                      height: 50,
                      textColor: Colors.black,
                      textFocusColor: Colors.white,
                      duration: Duration(milliseconds: 300),
                      inputType: TextInputType.text,
                      prefixIcon: Icon(Icons.email,color: Colors.black,),
                      suffixIcon: Icon(Icons.remove_red_eye),
                      placeholder: "password",
                      autofocus: true,
                      onTap: () {
                        print('Click');
                      },
                      onChanged: (text) {
                        print(text);
                      },
                      onSubmitted: (data) {
                        print(data.length);
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 10.0),
                    child:
                    BeautyTextfield(
                      width: double.maxFinite,
                      height: 50,
                      textColor: Colors.black,
                      textFocusColor: Colors.white,
                      obscureText: true,
                      duration: Duration(milliseconds: 300),
                      inputType: TextInputType.text,
                      prefixIcon: Icon(Icons.lock_outline,color: Colors.black,),
                      suffixIcon: Icon(Icons.remove_red_eye),
                      placeholder: "password",
                      autofocus: true,
                      onTap: () {
                        print('Click');
                      },
                      onChanged: (text) {
                        print(text);
                      },
                      onSubmitted: (data) {
                        print(data.length);
                      },
                    ),
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
                    alignment: Alignment.center,
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new FlatButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            color: Colors.white,
                            onPressed: () => (){},
                            child: new Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 20.0,
                                horizontal: 20.0,
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Expanded(
                                    child: Text(
                                      "LOGIN",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.cyan,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}